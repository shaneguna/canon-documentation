---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_486f1e80dc86a4a6f3800cfa5e47b34e -->
## Order details for API consumption.

> Example request:

```bash
curl "http://localhost/api/v1/transact-order" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/transact-order",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "status": 0,
    "code": 200,
    "message": "Reference no. cannot be empty"
}
```

### HTTP Request
`GET api/v1/transact-order`

`HEAD api/v1/transact-order`


<!-- END_486f1e80dc86a4a6f3800cfa5e47b34e -->
<!-- START_f162a3e441588d1c0c6ce8ae1c6fefb9 -->
## Order details for API consumption.

> Example request:

```bash
curl "http://localhost/api/v1/transact-order" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/transact-order",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/transact-order`


<!-- END_f162a3e441588d1c0c6ce8ae1c6fefb9 -->
<!-- START_af0010b80a703629ffb435efcf096473 -->
## Set status by API after order placement.

> Example request:

```bash
curl "http://localhost/api/v1/confirm-transaction" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/confirm-transaction",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v1/confirm-transaction`

`HEAD api/v1/confirm-transaction`


<!-- END_af0010b80a703629ffb435efcf096473 -->
<!-- START_62920cea9de4776bfa6f0f535cb13dba -->
## Set status by API after order placement.

> Example request:

```bash
curl "http://localhost/api/v1/confirm-transaction" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/confirm-transaction",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/confirm-transaction`


<!-- END_62920cea9de4776bfa6f0f535cb13dba -->
<!-- START_5548b396691e63e32ee5290b6068fbe2 -->
## API consumption for getting last 5 orders from a store.

> Example request:

```bash
curl "http://localhost/api/v1/get-recent-orders" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/get-recent-orders",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/get-recent-orders`


<!-- END_5548b396691e63e32ee5290b6068fbe2 -->
<!-- START_c126f815c06772b80621aa017eca8a19 -->
## API consumption for validating managers pin with store id

> Example request:

```bash
curl "http://localhost/api/v1/validate-managers-pin" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/validate-managers-pin",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/validate-managers-pin`


<!-- END_c126f815c06772b80621aa017eca8a19 -->
<!-- START_3a4ca384b1e31d4142a8328b68db24e9 -->
## Send a reset link to the given user.

> Example request:

```bash
curl "http://localhost/api/v1/forgot_password_email" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/forgot_password_email",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/forgot_password_email`


<!-- END_3a4ca384b1e31d4142a8328b68db24e9 -->
<!-- START_bf646b070ce6b0cfabad36764b94d59e -->
## Send a reset link to the given user with mobile number.

> Example request:

```bash
curl "http://localhost/api/v1/forgot_password_number" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/forgot_password_number",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/forgot_password_number`


<!-- END_bf646b070ce6b0cfabad36764b94d59e -->
<!-- START_8e817188ac4b7ad6070bcebc880b12ee -->
## Handles posting mail for account with same multiple mail detail.

> Example request:

```bash
curl "http://localhost/api/v1/forgot_password_email_multiple" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/forgot_password_email_multiple",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/forgot_password_email_multiple`


<!-- END_8e817188ac4b7ad6070bcebc880b12ee -->
<!-- START_ba8ba54c06c3c294dec81808c274c42c -->
## Send mail to user after placing new order.

> Example request:

```bash
curl "http://localhost/api/v1/new_order_customer_email" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/new_order_customer_email",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/new_order_customer_email`


<!-- END_ba8ba54c06c3c294dec81808c274c42c -->
<!-- START_62056fa4c8b5ca28963c5133fb549f4b -->
## Login the user.

> Example request:

```bash
curl "http://localhost/api/v1/attempt-sign-in" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/attempt-sign-in",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/attempt-sign-in`


<!-- END_62056fa4c8b5ca28963c5133fb549f4b -->
<!-- START_9a01b608adca281e13308a71226945be -->
## api/v1/get-store-list

> Example request:

```bash
curl "http://localhost/api/v1/get-store-list" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v1/get-store-list",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v1/get-store-list`


<!-- END_9a01b608adca281e13308a71226945be -->

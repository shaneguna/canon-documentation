@echo off
FOR /F "TOKENS=1 eol=/ DELIMS=/ " %%A IN ('DATE/T') DO SET dd=%%A
FOR /F "TOKENS=1,2 eol=/ DELIMS=/ " %%A IN ('DATE/T') DO SET mm=%%B
FOR /F "TOKENS=1,2,3 eol=/ DELIMS=/ " %%A IN ('DATE/T') DO SET yyyy=%%C
SET TODAY=%yyyy%%mm%%dd%
SET Log=C:\Users\CHECKOUT001\Desktop\cron
SET MYSQL_EXE="C:\wamp64\bin\mysql\mysql5.7.14\bin\mysql.exe"
SET DB_USER=root
SET DB_PWD=root
SET DB_NAME=kiosk_db_2
SET SQL_PIVOT_SOURCE="C:\Users\CHECKOUT001\Desktop\mina.sql.20170302"
echo Job run on %date% at %time% by %UserName% >> %Log%
%MYSQL_EXE% -u %DB_USER% -p%DB_PWD% %DB_NAME% < %SQL_PIVOT_SOURCE%  1>%Log  2>&1
echo. >> %Log%
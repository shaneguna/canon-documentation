---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_4dac6aeb410e66e97f1d9b19178406a3 -->
## Post new customer details.

> Example request:

```bash
curl "http://localhost/api/v2/post_new_order" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/post_new_order",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v2/post_new_order`


<!-- END_4dac6aeb410e66e97f1d9b19178406a3 -->
<!-- START_c9af84fbed8af1890554cc1d11334e97 -->
## API Consumption for order tracking.

> Example request:

```bash
curl "http://localhost/api/v2/track_order" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/track_order",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v2/track_order`


<!-- END_c9af84fbed8af1890554cc1d11334e97 -->
<!-- START_069d554ae79bc230735a230da2af6d5c -->
## Login the user.

> Example request:

```bash
curl "http://localhost/api/v2/attempt-sign-in" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/attempt-sign-in",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v2/attempt-sign-in`

`HEAD api/v2/attempt-sign-in`


<!-- END_069d554ae79bc230735a230da2af6d5c -->
<!-- START_cae5628c08d06cd104d9adbf2e88eb4e -->
## Login the user.

> Example request:

```bash
curl "http://localhost/api/v2/attempt-sign-in" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/attempt-sign-in",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v2/attempt-sign-in`


<!-- END_cae5628c08d06cd104d9adbf2e88eb4e -->
<!-- START_99523a7b96b50b1a0b04cfa75c35cabc -->
## API Consumption for checking product availability.

> Example request:

```bash
curl "http://localhost/api/v2/check-product-availability" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/check-product-availability",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v2/check-product-availability`


<!-- END_99523a7b96b50b1a0b04cfa75c35cabc -->
<!-- START_ced663bf55d30a0f31b26a17f8f88cf3 -->
## Get customer details

> Example request:

```bash
curl "http://localhost/api/v2/get-returning-customer" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/get-returning-customer",
    "method": "GET",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
null
```

### HTTP Request
`GET api/v2/get-returning-customer`

`HEAD api/v2/get-returning-customer`


<!-- END_ced663bf55d30a0f31b26a17f8f88cf3 -->
<!-- START_538d57bf4093b1171ab3a1165e53677a -->
## Post order of returning customer.

> Example request:

```bash
curl "http://localhost/api/v2/post_returning_order" \
-H "Accept: application/json"
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/v2/post_returning_order",
    "method": "POST",
    "headers": {
        "accept": "application/json"
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```


### HTTP Request
`POST api/v2/post_returning_order`


<!-- END_538d57bf4093b1171ab3a1165e53677a -->
